(function(){
	let name = prompt('Setting name:', '');
	if(name === null) return;
	let value = '';
	let lock = navigator.mozSettings.createLock();
	let setting = lock.get(name);
	function setValue(){
		alert('Current value: ' + value[name]);
		value = prompt('Setting value:', value[name]);
		if(value === null) return;
		if(!isNaN(value)) value = Number(value);
		lock = navigator.mozSettings.createLock();
		let pair = new Object();
		pair[name] = value;
		setting = lock.set(pair);
		setting.onsuccess = function(){
			alert('Done!');
		}
		setting.onerror = function(){
			alert('Error: ' + setting.error.name + ' ' +
				setting.error.message);
		}
	}
	setting.onsuccess = function(){
		value = setting.result;
		setValue();
	}
	settings.onerror = function(){
		setValue();
	}
})();
