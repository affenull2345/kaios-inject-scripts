function installPkg(packageFile) {
	navigator.mozApps.mgmt.import(packageFile).then(function(){
		alert('Installation successful!');
	}).catch(e=>{
		alert('Installation error: ' + e.name + ' ' + e.message);
	})
	let appGetter = navigator.mozApps.mgmt.getAll();
	appGetter.onsuccess = function() {
		let apps = appGetter.result;
		console.dir(apps);
	}
	appGetter.onerror = function(e) {
		console.dir(this.error);
	}
}
let picker = new MozActivity({name: 'pick'});
picker.onsuccess = function(){
	let file = this.result.blob
	confirm('Install ' + file.name + '?') && installPkg(file);
}
